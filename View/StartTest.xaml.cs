﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MZMT.View
{
	public partial class StartTest : ContentPage
	{
		public StartTest()
		{
			InitializeComponent ();
            btn.Clicked += Btn_Clicked;
		}

        private void Btn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new TestPage());
        }
    }
}
