﻿using AlphaChiTech.Virtualization;
using HelperLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MZMT.View
{
	public partial class TestPage : ContentPage
	{
		public TestPage ()
		{
			InitializeComponent ();

            listView.ItemsSource = new EObservableCollection<TestObject>(CreateSource());
		}

        private void Btn_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private IEnumerable<TestObject> CreateSource()
        {
            var list = new List<TestObject>();

            for(int i = 0; i<2000; i++)
            {
                list.Add(new TestObject { a=i.ToString(), b="bbbb", c="cccc", d="dddddddd"});
            }

            return list;
        }

        private class TestObject
        {
            public string a { get; set; }
            public string b { get; set; }
            public string c { get; set; }
            public string d { get; set; }
        }
    }
}
