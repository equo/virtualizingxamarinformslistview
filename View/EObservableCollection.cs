﻿using AlphaChiTech.Virtualization;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelperLib
{
    public class EObservableCollection<T> : VirtualizingObservableCollection<T>
    {
        public EObservableCollection(IEnumerable<T> Collection)
            : base(new PaginationManager<T>(new PagedCollectionProvider<T>(Collection)))
        { }
    }
}
