﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HelperLib
{

    public static class Helper
    {
        public static int IndexOf<T>(this IEnumerable<T> source, T value)
        {
            int index = 0;
            var comparer = EqualityComparer<T>.Default;
            foreach (T item in source)
            {
                if (comparer.Equals(item, value)) return index;
                index++;
            }
            return -1;
        }
    }

}
