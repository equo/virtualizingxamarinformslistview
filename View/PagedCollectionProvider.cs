﻿using AlphaChiTech.Virtualization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelperLib
{
    public class PagedCollectionProvider<T> : AlphaChiTech.Virtualization.IPagedSourceProvider<T>
    {
        private IEnumerable<T> Collection;

        public PagedCollectionProvider(IEnumerable<T> Collection)
        {
            this.Collection = Collection;
        }

        public int Count
        {
            get
            {
                if (Collection == null) return 0;

                var coll = Collection as ICollection<T>;
                if (coll != null)
                    return coll.Count;

                return Collection.Count();
            }
        }

        public PagedSourceItemsPacket<T> GetItemsAt(int pageoffset, int count, bool usePlaceholder)
        {
            if (Collection == null) return null;

            return new PagedSourceItemsPacket<T>()
            {
                LoadedAt = DateTime.Now,
                Items = Collection.Skip(pageoffset).Take(count)
            };
        }

        public int IndexOf(T item)
        {
            if (Collection == null) return -1;

            var list = Collection as IList<T>;
            if (list != null)
                return list.IndexOf(item);

            return Collection.IndexOf(item);
        }

        public void OnReset(int count)
        {

        }
    }
}
