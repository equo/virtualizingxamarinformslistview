﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MZMT.View
{
	public partial class App : Application
	{
		public App ()
		{
            InitializeComponent ();

            InitVirtualizationManager();

            MainPage = new NavigationPage(new StartTest());
        }

        private void InitVirtualizationManager()
        {
            if (!AlphaChiTech.Virtualization.VirtualizationManager.IsInitialized)
            {
                AlphaChiTech.Virtualization.VirtualizationManager.Instance.UIThreadExcecuteAction = (a) => Device.BeginInvokeOnMainThread(a);

                Device.StartTimer(
                    TimeSpan.FromSeconds(1),
                    () =>
                    {
                        AlphaChiTech.Virtualization.VirtualizationManager.Instance.ProcessActions();
                        return true;
                    });
            }
        }


        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
